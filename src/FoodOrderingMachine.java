import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    int total=0;
    void order(String foodName , int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + foodName + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            String select[]={"1","2","3","4","5","6","7","8","9","10"};
            String currentText = orderedList.getText();

            Object count = JOptionPane.showInputDialog(
                    null,
                    "How many would you like to order?",
                    "Number of order",
                    JOptionPane.INFORMATION_MESSAGE,
                    null,
                    select,
                    select[0]
            );


            if(count != null){
                String countStr = count.toString();
                int countInt = Integer.parseInt(countStr);
                total += price * countInt;
                int sum = price * countInt;

                orderedList.setText(currentText + foodName + " ×" + count + "  " + sum + "yen\n");

                JOptionPane.showMessageDialog(
                        null,
                        "Order for " + foodName + " ×" + count + " received."
                );

                totalPrice.setText("Total          " + total + "yen");
            }
        }
    }
    private JPanel root;
    private JLabel topLabel;
    private JButton saladButton;
    private JButton kimchiButton;
    private JButton shortRibButton;
    private JButton outsideSkirtButton;
    private JButton tongueButton;
    private JButton loinButton;
    private JTextArea orderedList;
    private JButton checkOutButton;
    private JLabel totalPrice;

    public FoodOrderingMachine() {
        saladButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salad",770);
            }
        });
        saladButton.setIcon(new ImageIcon(
                this.getClass().getResource("Salad.jpg")
        ));

        kimchiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kimchi",1320);
            }
        });
        kimchiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Kimchi.jpg")
        ));

        tongueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tongue",2530);
            }
        });
        tongueButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tongue.jpg")
        ));

        outsideSkirtButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("OutsideSkirt",2200);
            }
        });
        outsideSkirtButton.setIcon(new ImageIcon(
                this.getClass().getResource("OutsideSkirt.jpg")
        ));

        shortRibButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ShortRib",3520);
            }
        });
        shortRibButton.setIcon(new ImageIcon(
                this.getClass().getResource("ShortRib.jpg")
        ));

        loinButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Loin",3300);
            }
        });
        loinButton.setIcon(new ImageIcon(
                this.getClass().getResource("Loin.jpg")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkOut = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if(checkOut == 0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you. The total price is " + total + " yen."
                    );

                    orderedList.setText("");
                    total=0;
                    totalPrice.setText("Total          " + total + "yen");
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
